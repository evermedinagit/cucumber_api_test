package com.menstoys.cucumber_api_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CucumberApiTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CucumberApiTestApplication.class, args);
		
		System.out.println("Hello from logger C...");

		System.out.println("comment from A.");
		
		System.out.println("comment from B..");

	}

}
